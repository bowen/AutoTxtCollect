package com.bowen.txt;


import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AutoTxtCollect {
	public static void main(String[] args) {
		String url = "http://www.cangqionglongqi.com/dagongwushishenghuolu/";
		try {
			String uu = getDoc("http://api.jiosss.com/getT/html/17").select("url").text();
			System.out.println(uu);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	/*	AutoTxtCollect atc = new AutoTxtCollect();
		try{
		if(url.contains("cangqionglongqi.com"))
		{
			String name = atc.getTxt1(url);
			Sendmail.send(name, null, "duguhu007@qq.com", name+".txt");
//			Sendmail.send("打工巫师生活录", "打工巫师生活录", "duguhu007@qq.com","打工巫师生活录.txt");
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}*/
	}
	
	public String getTxt1(String url) throws Exception {
		Document doc = getDoc(url);
		String fileName = doc.select("#info h1").text();
		Weblog.log(fileName);
		FileWriter txt = new FileWriter(fileName+".txt");
		Elements al = doc.select("#list dd a");
		Weblog.log(al.size());
		for (Element a : al) {
			String href = url + a.attr("href");
			Weblog.log(href);
			Document adoc = getDoc(href);
			Elements atitle = adoc.select(".bookname h1");
			Weblog.log(atitle);
			Elements con = adoc.select("#content");
			Weblog.log(con);
			txt.write(atitle.text() + "\r\n" + con.text()+"\r\n");
			Thread.sleep(500);
		}
		txt.close();
		return fileName;
	}

	public static Document getDoc(String href) throws UnsupportedEncodingException, IOException {
		Exception exception = null;
		Document doc = null;
		for (int i = 0; i < 3; i++) {
			try {
				Weblog.log("get doc " + href);
				doc = Jsoup.connect(href).header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8").header("Accept-Language", "en-US,us;q=0.8").userAgent("Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0").timeout(15000).get();
				exception = null;
				break;
			} catch (Exception e) {
				exception = e;
			}
		}
		if (exception != null) {
			throw new RuntimeException(exception);
		}
		return doc;
	}
}
class Weblog
{
	private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static void log(Object msg)
	{
		System.out.println(String.format("[%s] %s", date.format(new Date()),msg));
	}
}