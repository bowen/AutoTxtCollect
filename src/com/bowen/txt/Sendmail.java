package com.bowen.txt;

import java.io.FileOutputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;


public class Sendmail {

	private static String mailUName=""
	private static String mailPwd="";
    /**
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
    	
    }
    
    public static void send(String subobject,String html,String toMail,String ...files) throws Exception
    {
    	 Properties prop = new Properties();
         prop.setProperty("mail.host", "smtp.mxhichina.com");
         prop.setProperty("mail.transport.protocol", "smtp");
         prop.setProperty("mail.smtp.port", "25");
         prop.setProperty("mail.smtp.auth", "true");
         //使用JavaMail发送邮件的5个步骤
         //1、创建session
         Session session = Session.getInstance(prop);
         //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
         session.setDebug(true);
         //2、通过session得到transport对象
         Transport ts = session.getTransport();
         //3、连上邮件服务器
         ts.connect("smtp.mxhichina.com", mailUName, mailPwd);
         //4、创建邮件
         Message message = createMixedMail(session,subobject,html,toMail,files);
         //5、发送邮件
         ts.sendMessage(message, message.getAllRecipients());
         ts.close();
    }
    
    public static MimeMessage createMixedMail(Session session,String subobject,String html,String toMail,String ...files) throws Exception {
        //创建邮件
        MimeMessage message = new MimeMessage(session);
        
        //设置邮件的基本信息
        message.setFrom(new InternetAddress(mailUName));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(toMail));
        if(subobject==null)
        {
        	subobject = "带附件的邮件";
        }
        message.setSubject(subobject);
        
        //正文
        MimeBodyPart text = new MimeBodyPart();
        text.setContent(html,"text/html;charset=UTF-8");
        

        
      //描述关系:正文和图片
        MimeMultipart mp1 = new MimeMultipart();
        mp1.addBodyPart(text);
//        mp1.addBodyPart(image);
        mp1.setSubType("related");
        
        //描述关系:正文和附件
        MimeMultipart mp2 = new MimeMultipart();
        
        if(files.length>0)
        {
        	for (String file : files) {
        		MimeBodyPart attach = new MimeBodyPart();
                DataHandler dh = new DataHandler(new FileDataSource(file));
                attach.setDataHandler(dh);
                attach.setFileName(MimeUtility.encodeText(dh.getName()));
                mp2.addBodyPart(attach);
			}
        }

        
        //代表正文的bodypart
        MimeBodyPart content = new MimeBodyPart();
        content.setContent(mp1);
        mp2.addBodyPart(content);
        mp2.setSubType("mixed");
        
        message.setContent(mp2);
        message.saveChanges();
        
//        message.writeTo(new FileOutputStream("E:\\MixedMail.eml"));
        //返回创建好的的邮件
        return message;
    }
}